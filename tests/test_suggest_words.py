#!/usr/bin/python3

import enchant

wdlst = [ "hello", "shaah", "subax"]
dic = enchant.Dict("so_SO")
for wd in wdlst:
    dic.check(wd)
    print("input word = {0}, Suggestions => {1}".format(wd, dic.suggest(wd)))
